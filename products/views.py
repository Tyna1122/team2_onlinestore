from django.shortcuts import render
from django.views.generic import ListView

from django.http import HttpResponse

from .models import Products

# Create your views here.
class ProductListView(ListView):
    model = Products   
    template_name = 'home.html'