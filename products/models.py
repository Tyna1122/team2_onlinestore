from django.db import models

# Create your models here.
class Products(models.Model):
    productName = models.CharField(max_length=128)
    quantity = models.IntegerField()
    category = models.CharField(max_length=50)
    subcategory = models.CharField(max_length=50)
    price = models.IntegerField()
    image = models.ImageField(upload_to='images/')

    def __str__(self):
        return f"{self.id}, {self.title}"
